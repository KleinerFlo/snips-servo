#!/usr/bin/env python3

from hermes_python.hermes import Hermes

# imported to get type check and IDE completion
from hermes_python.ontology.dialogue.intent import IntentMessage

import RPi.GPIO as GPIO
# GPIO Pin festlegen
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
 
# PWM initialisieren 
p = GPIO.PWM(23, 50)  # frequency=50Hz
p.start(6.75)



class Servo:
    """class used to wrap action code with mqtt connection
       please change the name referring to your application
    """

    def __init__(self):
        # get the configuration if needed

        self.config = None

        # start listening to MQTT
        self.start_blocking()

 
    @staticmethod
    def servo_callback(hermes: Hermes,
                          intent_message: IntentMessage):
        
        result_sentence = "Ich habe leider nicht verstanden was du meinst."
        if (len(intent_message.slots) != 0):
            winkel = intent_message.slots.richtung.first().value # Winkel aus Slot "Richtung" extrahieren 
            if (winkel == 'links'):
                try:
                    p.ChangeDutyCycle(11.5) # PWM ändern / Motor stellen
                    result_sentence = "Ich lenke nach links"  # Antwortsatz den die TTS Engine vorliest.
                except:
                    pass
                    p.stop()
                    GPIO.cleanup()
                
            elif (winkel == 'rechts'):
                try:
                    p.ChangeDutyCycle(2.5) # PWM ändern / Motor stellen
                    result_sentence = "Ich lenke nach rechts"  
                except:
                    pass
                    p.stop()
                    GPIO.cleanup()
            elif (winkel == 'gerade'):
                try:
                    p.ChangeDutyCycle(6.75) # PWM ändern / Motor stellen
                    result_sentence = "Ich lenke gerade aus" 
                except:
                    pass
                    p.stop()
                    GPIO.cleanup()
        else:
            result_sentence = "Bitte versuch es nocheinmal"
            
        #Session beenden
        hermes.publish_end_session(intent_message.session_id, result_sentence)

    # Den Intents die richtige Callback Funktion zuweisen und MQTT Verbindung aufbauen
    def start_blocking(self):
        with Hermes("localhost:1883") as h:
            h.subscribe_intent('FloD:Winkel', self.servo_callback)\
            .loop_forever()


if __name__ == "__main__":
    Servo()


